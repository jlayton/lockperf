#!/bin/bash

PATH=$PATH:.

destdir=./results
tests="posix01 posix02 posix03 flock01 flock02"
numtests=100
arg="/tmp/lockperf"

rev=`uname -r`

mkdir -p $destdir

for ltest in $tests; do
	for i in `seq 1 $numtests`; do
		rm -rf $arg
		echo "$ltest $i"
		sync
		${ltest} ${arg} >> ${destdir}/${ltest}-${rev}.out
	done
done

