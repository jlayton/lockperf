/*
 * BSD lock performance test 1
 *
 * Fork off a given number of children who attempt to repeatedly acquire an
 * exclusive BSD lock and then release it for a given number of times. Time
 * this to get a rough indication of locking performance for a single,
 * contended whole-file lock.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/file.h>

#include "timespec.h"

#define NRPROC (128)
#define NRLOCK (10240)

static struct timespec *diff;

static int
lockunlock(const char *name, int nrlock, struct timespec *slot)
{
	int fd, i, ret = 0;
	struct timespec start, end;

	fd = open(name, O_CREAT|O_RDWR, 0644);
	if (fd < 0) {
		perror("open");
		return fd;
	}

	ret = clock_gettime(CLOCK_MONOTONIC_RAW, &start);
	if (ret) {
		perror("clock_gettime");
		return ret;
	}

	for (i = 0; i < nrlock; ++i) {
		ret = flock(fd, LOCK_EX);
		if (ret < 0) {
			perror("flock");
			break;
		}

		ret = flock(fd, LOCK_UN);
		if (ret < 0) {
			perror("flock");
			break;
		}

	}

	clock_gettime(CLOCK_MONOTONIC_RAW, &end);
	if (ret) {
		perror("clock_gettime");
		return ret;
	}

	close(fd);
	*slot = timespec_sub(end, start);
	return ret;
}

void
usage(char *prog)
{
	printf("usage: %s [-n nr_procs] [-l nr_locks] filename\n", prog);
}

int
main(int argc, char **argv)
{
	int i, opt, valid = 0;
	int nproc = NRPROC;
	int nlock = NRLOCK;
	pid_t *pids;
	struct timespec total = { .tv_sec = 0,
				  .tv_nsec = 0 };

	while ((opt = getopt(argc, argv, "l:n:")) != -1) {
		switch (opt) {
		case 'l':
			nlock = atoi(optarg);
			break;
		case 'n':
			nproc = atoi(optarg);
			break;
		default:
			usage(argv[0]);
			return 1;
		}
	}

	if (!argv[optind]) {
		usage(argv[0]);
		return 1;
	}

	pids = calloc(nproc, sizeof(pid_t));
	if (!pids) {
		fprintf(stderr, "Unable to allocate pids array!");
		return 1;
	}

	diff = mmap(0, nproc * sizeof(*diff), PROT_READ | PROT_WRITE,
			MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	if (diff == (struct timespec *)-1) {
		fprintf(stderr, "Unable to allocate timespec array!");
		return 1;
	}

	for (i = 0; i < nproc; ++i) {
		pids[i] = fork();
		if (!pids[i])
			return lockunlock(argv[optind], nlock, &diff[i]);
	}

	for (i = 0; i < nproc; ++i) {
		int status;

		if (pids[i] < 0) {
			fprintf(stderr, "process %d failed to fork\n", i);
			continue;
		}
		if (waitpid(pids[i], &status, 0) < 0) {
			fprintf(stderr, "unable to reap pid %d\n", pids[i]);
			continue;
		}
		if (!WIFEXITED(status) || WEXITSTATUS(status)) {
			fprintf(stderr, "pid %d exited abnormally(0x%x)\n", pids[i],status);
			continue;
		}
		total = timespec_add(total, diff[i]);
		++valid;
	}

	if (valid != nproc) {
		fprintf(stderr, "Some children didn't run properly -- "
				"requested %d but only got %d\n", nproc, valid);
		return 1;
	}

	printf("%ld.%09ld\n", total.tv_sec, total.tv_nsec);
	return 0;
}
