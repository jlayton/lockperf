/*
 * Copyright Rusty Russell, 2013, IBM Corporation
 * Copyright Jeff Layton, 2013
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <regex.h>
#include <assert.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <signal.h>
#include <sys/wait.h>

#include "timespec.h"

#define DEFAULT_PROCESSES 5000
#define DEFAULT_ITERATIONS 100

/* Taken from TDB: LGPLv3 */
static int fcntl_lock(int fd, int rw, off_t off, off_t len, bool waitflag)
{
	struct flock fl;

	fl.l_type = rw;
	fl.l_whence = SEEK_SET;
	fl.l_start = off;
	fl.l_len = len;
	fl.l_pid = 0;

	if (waitflag)
		return fcntl(fd, F_SETLKW, &fl);
	else
		return fcntl(fd, F_SETLK, &fl);
}

static int fcntl_unlock(int fd, off_t off, off_t len)
{
	struct flock fl;
	fl.l_type = F_UNLCK;
	fl.l_whence = SEEK_SET;
	fl.l_start = off;
	fl.l_len = len;
	fl.l_pid = 0;

	return fcntl(fd, F_SETLKW, &fl);
}

static void
kill_children()
{
	siginfo_t	infop;

	signal(SIGINT, SIG_IGN);
	kill(0, SIGINT);
	while (waitid(P_ALL, 0, &infop, WEXITED) != -1);
}

static void
sighandler(int sig __attribute__((unused)))
{
	kill_children();
	exit(0);
}

static int do_child(int lockfd, int i, int to_lockers, int from_lockers)
{
	unsigned char c;

	while (read(to_lockers, &c, 1) == 1) {
		if (c != '\0')
			return 0;
		if (fcntl_lock(lockfd, F_WRLCK, i, 1, true) != 0)
			err(1, "Locking");
		/* Tell parent we got it! */
		if (write(from_lockers, &c, 1) != 1)
			err(1, "Writing to parent");
		usleep(1000);
		fcntl_unlock(lockfd, i, 1);
	}
	return 0;
}

static int
usage(char *argv0)
{
	errx(1, "Usage: %s [-i iterations] [-n nr_children] <filename>", argv0);
}

int main(int argc, char *argv[])
{
	int num = DEFAULT_PROCESSES, i, opt;
	int iter = DEFAULT_ITERATIONS;
	const char *lockfile;
	char *fill;
	int lockfd, to_lockers[2], from_lockers[2];
	struct timespec start, end;
	struct timespec total;
	struct rlimit rlim;

	total.tv_sec = 0;
	total.tv_nsec = 0;

	while ((opt = getopt(argc, argv, "i:n:")) != -1) {
		switch (opt) {
		case 'i':
			iter = atoi(optarg);
			break;
		case 'n':
			num = atoi(optarg);
			break;
		default:
			usage(argv[0]);
		}
	}

	lockfile = argv[optind];
	if (!lockfile)
		usage(argv[0]);

	/* bump rlimit */
	if (getrlimit(RLIMIT_NPROC, &rlim))
		err(1, "getrlimit");
	rlim.rlim_cur = rlim.rlim_max;
	if (setrlimit(RLIMIT_NPROC, &rlim))
		err(1, "setrlimit");

	if (pipe(to_lockers))
		err(1, "pipe (to_lockers)");
	if (pipe(from_lockers))
		err(1, "pipe (from_lockers)");

	lockfd = open(lockfile, O_CREAT|O_RDWR, 0644);
	if (lockfd < 0)
		err(1, "Opening %s", lockfile);

	signal(SIGINT, sighandler);

	for (i = 0; i < num; i++) {
		switch (fork()) {
		case 0:
			signal(SIGINT, SIG_DFL);
			return do_child(lockfd, i, to_lockers[0], from_lockers[1]);
		case -1:
			err(1, "fork failed");
		}
	}

	close(to_lockers[0]);
	close(from_lockers[1]);

	fill = calloc(num, 1);

	while (iter--) {
		if (fcntl_lock(lockfd, F_WRLCK, 0, num, true) != 0)
			err(1, "Locking %u bytes in %s", num, lockfile);

		/* OK, now wake all the kids: they block on lock. */
		i = 0;
		do {
			int ret = write(to_lockers[1], fill + i, num - i);

			if (ret < 0)
				err(1, "writing to wake up locker children");
			i += ret;
		} while (i < num);

		usleep(1000);

		if (clock_gettime(CLOCK_MONOTONIC_RAW, &start))
			err(1, "Getting start time");

		fcntl_unlock(lockfd, 0, num);
		for (i = 0; i < num; i++)
			if (read(from_lockers[0], fill, 1) != 1)
				err(1, "Reading from locker children");

		if (clock_gettime(CLOCK_MONOTONIC_RAW, &end))
			err(1, "Getting start time");

		total = timespec_add(total, timespec_sub(end, start));
	}

	printf("%ld.%09ld\n", total.tv_sec, total.tv_nsec);
	kill_children();
	return 0;
}
