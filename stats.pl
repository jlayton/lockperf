#!/usr/bin/perl -w
#
# Postprocess output from locktest runs
#
# Feed output into this script like so:
#
#     $ ./stats.pl < posix01-foo-bar-baz.out
#

$num = 0;
$total = 0;

while(<>) {
	chomp;
	$val[$num] = $_;
	$total += $val[$num];
	$num++;
}

$mean = $total / $num;
$min = $val[0];
$max = 0;

for ($i = 0; $i < $num; $i++) {
	$squared_diff += ($val[$i] - $mean) ** 2;
	if ($val[$i] < $min) {
		$min = $val[$i];
	}
	if ($val[$i] > $max) {
		$max = $val[$i];
	}
}

$variance =  $squared_diff / $num;
$std_dev = sqrt($variance);

#print "Num values:\t$num\n";
#print "Total:\t\t$total\n";
print "Min:\t\t$min\n";
print "Max:\t\t$max\n";
print "Mean:\t\t$mean\n";
#print "Variance:\t$variance\n";
print "Std. Dev.:\t$std_dev\n";
